#ifndef SAFE_FUNCTIONAL_HPP
#define SAFE_FUNCTIONAL_HPP

#include<thread>
#include<mutex>
#include<optional>
#include<future>
#include"safeaccess.hpp"


namespace safe_functional {

  template<typename F1, typename ...Args1>
  class cps {
  public:
    using return_t = std::invoke_result_t<F1,Args1...>;
    cps(F1 f, Args1... args)
      :future(std::async(std::launch::async, f, args...))
    {
    }

    template<typename F2>
    auto operator >> (F2 c){
      return c(future.get());
    }

    return_t get_result() {
      return future.get();
    }

  private:
    std::future<return_t> future;
  };

  template<typename C, typename F>
  auto operator << (C c, cps<F>& f) {
    return f >> c;
  }

};


namespace sf = safe_functional;

#endif
